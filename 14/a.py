input_file_name = "input.txt"

columns = None

with open(input_file_name, encoding="utf-8") as input_file:
  raw_map = [line.strip() for line in input_file]
  if raw_map[-1] == "":
    raw_map.pop()
  columns = ["".join([line[i] for line in raw_map]) for i in range(len(raw_map[0]))]

load = 0

for column in columns:
  sections = column.split("#")
  distance_from_south = len(column)
  for section in sections:
    rocks = section.count("O")
    for i in range(rocks):
      load += distance_from_south - i
    distance_from_south -= len(section)
    distance_from_south -= 1

print(load)

# 296560 is too high