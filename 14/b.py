input_file_name = "input.txt"

columns = None

def slide_to_start(row):
  sections = row.split("#")
  new_sections = []
  for section in sections:
    rock_count = section.count("O")
    space_count = len(section) - rock_count
    new_sections.append("".join(["O" for i in range(rock_count)]) + "".join(["." for i in range(space_count)]))
  return "#".join(new_sections)

def rotate_cw(grid):
  return ["".join([line[i] for line in reversed(grid)]) for i in range(len(grid[0]))]

def calculate_load(grid):
  load = 0
  for i in range(len(grid)):
    load += grid[i].count("O") * (len(grid) - i)
  return load

with open(input_file_name, encoding="utf-8") as input_file:
  grid = [line.strip() for line in input_file]
  if grid[-1] == "":
    grid.pop()

# rotate so N is at left
for i in range(3):
  grid = rotate_cw(grid)

# presumably we'll start repeating positions at some point
cycle_end_positions = {}

def grid_to_string(grid):
  return "\n".join(grid)

# do actual cycle
total_cycles = 1000000000
for i in range(total_cycles):
  for j in range(4):
    new_grid = []
    for row in grid:
      new_grid.append(slide_to_start(row))
    grid = new_grid
    grid = rotate_cw(grid)
  grid_string = grid_to_string(grid)
  #print("{}:\n{}\n".format(i,grid_string))
  if grid_string in cycle_end_positions:
    cycle_that_first_gave_this_position = cycle_end_positions[grid_string]
    #print("{} is repeat of {}".format(i, cycle_that_first_gave_this_position))
    repeat_length = i - cycle_that_first_gave_this_position
    remainder_after_cycles = (total_cycles - i - 1) % repeat_length
    cycle_final_position_is_repeat_of = cycle_that_first_gave_this_position + remainder_after_cycles
    final_position = None
    for (position, cycle) in cycle_end_positions.items():
      if cycle == cycle_final_position_is_repeat_of:
        final_position = position.split("\n")
        break
    grid = final_position
    break
  else:
    cycle_end_positions[grid_string] = i

# rotate once more so N ends up at top
grid = rotate_cw(grid)

#print("final:\n{}\n".format(grid_to_string(grid)))

# count load!
print(calculate_load(grid))