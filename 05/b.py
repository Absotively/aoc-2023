input_file_name = "input.txt"

class value_range:
  def __init__(this, start, size):
    this.start = start
    this.size = size

  def split(this, size_one):
    return (value_range(this.start, size_one), value_range(this.start + size_one, this.size - size_one))

class value_set:
  def __init__(this):
    this.ranges = []

class conversion:
  def __init__(this, destination_range_start, source_range_start, range_size):
    this.destination_range_start = destination_range_start
    this.source_range_start = source_range_start
    this.range_size = range_size
  
  def can_convert(this, source_value_range):
    if source_value_range.start < this.source_range_start:
      return False
    if source_value_range.start + source_value_range.size - this.source_range_start > this.range_size:
      return False
    return True
  
  def convert(this, source_value_range):
    return value_range(source_value_range.start - this.source_range_start + this.destination_range_start, source_value_range.size)

class conversion_set:
  def __init__(this, source_type, destination_type):
    this.source_type = source_type
    this.destination_type = destination_type
    this.conversions = []
  
  def convert(this, source_value_set):
    ranges_to_process = source_value_set.ranges.copy()
    processed_ranges = []
    while len(ranges_to_process) > 0:
      range_being_processed = ranges_to_process.pop()
      range_modified = False
      for conversion in this.conversions:
        if conversion.source_range_start > range_being_processed.start and conversion.source_range_start < range_being_processed.start + range_being_processed.size:
          ranges_to_process.extend(range_being_processed.split(conversion.source_range_start - range_being_processed.start))
          range_modified = True
          break
        elif conversion.source_range_start + conversion.range_size > range_being_processed.start and conversion.source_range_start + conversion.range_size < range_being_processed.start + range_being_processed.size:
          ranges_to_process.extend(range_being_processed.split(conversion.source_range_start + conversion.range_size - range_being_processed.start))
          range_modified = True
          break
      if not range_modified:
        processed_ranges.append(range_being_processed)
    converted_ranges = value_set()
    for range_being_converted in processed_ranges:
      range_converted = False
      for conversion in this.conversions:
        if conversion.can_convert(range_being_converted):
          converted_ranges.ranges.append(conversion.convert(range_being_converted))
          range_converted = True
          break
      if not range_converted:
        converted_ranges.ranges.append(range_being_converted)

    return converted_ranges

all_conversion_sets = []

def fully_convert(source_type, destination_type, source_value):
  current_type = source_type
  current_value = source_value
  while current_type != destination_type:
    for conversion_set in all_conversion_sets:
      if conversion_set.source_type == current_type:
        current_value = conversion_set.convert(current_value)
        current_type = conversion_set.destination_type
        break
  return current_value

initial_seeds = value_set()
current_conversion_set = None

with open(input_file_name, encoding="utf-8") as input_file:
  for line in input_file:
    if line.startswith("seeds: "):
      seed_line_values = [int(n) for n in line[7:].strip().split(" ")]
      for i in range(len(seed_line_values)//2):
        initial_seeds.ranges.append(value_range(seed_line_values[2*i], seed_line_values[2*i+1]))
    
    elif line.endswith(" map:\n"):
      (source_type, _, destination_type) = line[:-6].split("-")
      current_conversion_set = conversion_set(source_type, destination_type)
      all_conversion_sets.append(current_conversion_set)
    
    elif len(line.strip()) > 0:
      nums = [int(n) for n in line.strip().split(" ")]
      current_conversion_set.conversions.append(conversion(*nums))


locations = fully_convert("seed", "location", initial_seeds)

lowest_location_number = None
for locations_range in locations.ranges:
  if lowest_location_number is None or locations_range.start < lowest_location_number:
    lowest_location_number = locations_range.start

print(lowest_location_number)