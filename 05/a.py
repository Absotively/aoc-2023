input_file_name = "input.txt"

class conversion:
  def __init__(this, destination_range_start, source_range_start, range_size):
    this.destination_range_start = destination_range_start
    this.source_range_start = source_range_start
    this.range_size = range_size
  
  def can_convert(this, source_value):
    if source_value < this.source_range_start:
      return False
    if source_value - this.source_range_start > this.range_size:
      return False
    return True
  
  def convert(this, source_value):
    return source_value - this.source_range_start + this.destination_range_start

class conversion_set:
  def __init__(this, source_type, destination_type):
    this.source_type = source_type
    this.destination_type = destination_type
    this.conversions = []
  
  def convert(this, source_value):
    for conversion in this.conversions:
      if conversion.can_convert(source_value):
        return conversion.convert(source_value)
    return source_value
  
all_conversion_sets = []

def fully_convert(source_type, destination_type, source_value):
  current_type = source_type
  current_value = source_value
  while current_type != destination_type:
    for conversion_set in all_conversion_sets:
      if conversion_set.source_type == current_type:
        current_value = conversion_set.convert(current_value)
        current_type = conversion_set.destination_type
        break
  return current_value

initial_seeds = []
current_conversion_set = None

with open(input_file_name, encoding="utf-8") as input_file:
  for line in input_file:
    if line.startswith("seeds: "):
      initial_seeds = [int(n) for n in line[7:].strip().split(" ")]
    
    elif line.endswith(" map:\n"):
      (source_type, _, destination_type) = line[:-6].split("-")
      current_conversion_set = conversion_set(source_type, destination_type)
      all_conversion_sets.append(current_conversion_set)
    
    elif len(line.strip()) > 0:
      nums = [int(n) for n in line.strip().split(" ")]
      current_conversion_set.conversions.append(conversion(*nums))

lowest_location_number = None

for seed in initial_seeds:
  location = fully_convert("seed", "location", seed)
  if lowest_location_number is None or location < lowest_location_number:
    lowest_location_number = location

print(lowest_location_number)

# 56204204 is too low