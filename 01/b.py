input_file_name = "input.txt"

sum = 0

digit_names = {
  "zero": 0,
  "one": 1,
  "two": 2,
  "three": 3,
  "four": 4,
  "five": 5,
  "six": 6,
  "seven": 7,
  "eight": 8,
  "nine": 9
}

with open(input_file_name, encoding="utf-8") as input_file:
    for line in input_file:
        first_digit = None
        last_digit = None
        for i in range(len(line)):
            digit = None
            
            if line[i].isdigit():
                digit = int(line[i])
            else:
                for (name, value) in digit_names.items():
                    if line[i:].startswith(name):
                        digit = value
                        break
            
            if digit is not None:
                if first_digit == None:
                    first_digit = digit
                last_digit = digit
            
        sum += 10 * first_digit + last_digit

print(sum)