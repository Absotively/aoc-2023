input_file_name = "input.txt"

sum = 0

with open(input_file_name, encoding="utf-8") as input_file:
    for line in input_file:
        first_digit = None
        last_digit = None
        for char in line:
            if char.isdigit():
                if first_digit == None:
                    first_digit = char
                last_digit = char
        sum += 10 * int(first_digit) + int(last_digit)

print(sum)