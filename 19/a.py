input_file_name = "input.txt"

class part:
    def __init__(this, input_line):
        ratings = input_line.strip()[1:-1].split(",")
        this.x = int(ratings[0][2:])
        this.m = int(ratings[1][2:])
        this.a = int(ratings[2][2:])
        this.s = int(ratings[3][2:])

class test:
    def __init__(this, definition):
        if definition == "":
            this.type = "1"
        else:
            this.rating = definition[0]
            this.type = definition[1]
            this.value = int(definition[2:])
    def check(this, part):
        if this.type == "1":
            return True
        match this.rating:
            case "x":
                value_testing = part.x
            case "m":
                value_testing = part.m
            case "a":
                value_testing = part.a
            case "s":
                value_testing = part.s
        if this.type == "<":
            return value_testing < this.value
        elif this.type == ">":
            return value_testing > this.value
        else:
            print("Unknown test type: {}".format(this.type))
            exit()

class rule:
    def __init__(this, definition):
        if ":" not in definition:
            this.test = test("")
            this.destination = definition
        else:
            rule_text, this.destination = definition.split(":")
            this.test = test(rule_text)
    def apply(this, part):
        if this.test.check(part):
            return this.destination
        else:
            return False

class workflow:
    def __init__(this, definition):
        this.name, rule_defs = definition.strip()[:-1].split("{")
        this.rules = [rule(rule_def) for rule_def in rule_defs.split(",")]
    def apply(this, part):
        for rule in this.rules:
            destination = rule.apply(part)
            if destination:
                return destination

all_workflows = {}
ratings_sum = 0

with open(input_file_name, encoding="utf-8") as input_file:
    workflow_defs, part_defs = input_file.read().strip().split("\n\n")
    for workflow_def in workflow_defs.split("\n"):
        w = workflow(workflow_def)
        all_workflows[w.name] = w
    for part_def in part_defs.split("\n"):
        p = part(part_def)
        destination = "in"
        while destination != "A" and destination != "R":
            destination = all_workflows[destination].apply(p)
        if destination == "A":
            ratings_sum += p.x + p.m + p.a + p.s

print(ratings_sum)
