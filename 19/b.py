input_file_name = "input.txt"

class part_type:
    def __init__(this):
        this.x_min = 1
        this.x_max = 4000
        this.m_min = 1
        this.m_max = 4000
        this.a_min = 1
        this.a_max = 4000
        this.s_min = 1
        this.s_max = 4000
        this.destination = "in"
    def split(this, rating, new_min):
        a = part_type()
        b = part_type()
        a.x_min = b.x_min = this.x_min
        a.x_max = b.x_max = this.x_max
        a.m_min = b.m_min = this.m_min
        a.m_max = b.m_max = this.m_max
        a.a_min = b.a_min = this.a_min
        a.a_max = b.a_max = this.a_max
        a.s_min = b.s_min = this.s_min
        a.s_max = b.s_max = this.s_max
        a.destination = b.destination = this.destination
        match(rating):
            case "x":
                if this.x_max < new_min:
                    b = None
                elif this.x_min > new_min:
                    a = None
                else:
                    a.x_max = new_min - 1
                    b.x_min = new_min
            case "m":
                if this.m_max < new_min:
                    b = None
                elif this.m_min > new_min:
                    a = None
                else:
                    a.m_max = new_min - 1
                    b.m_min = new_min
            case "a":
                if this.a_max < new_min:
                    b = None
                elif this.a_min > new_min:
                    a = None
                else:
                    a.a_max = new_min - 1
                    b.a_min = new_min
            case "s":
                if this.s_max < new_min:
                    b = None
                elif this.s_min > new_min:
                    a = None
                else:
                    a.s_max = new_min - 1
                    b.s_min = new_min
        return (a,b)
    def __repr__(this):
        return ("({} <= x <= {}; {} <= m <= {}; {} <= a <= {}; {} <= s <= {})".format(this.x_min, this.x_max, this.m_min, this.m_max, this.a_min, this.a_max, this.s_min, this.s_max))

class test:
    def __init__(this, definition):
        if definition == "":
            this.type = "1"
        else:
            this.rating = definition[0]
            this.type = definition[1]
            this.value = int(definition[2:])
    def apply(this, part_type):
        if this.type == "1":
            return {"pass": part_type, "fail": None}
        if this.type == "<":
            new_part_types = part_type.split(this.rating, this.value)
            return {"pass": new_part_types[0], "fail": new_part_types[1]}
        elif this.type == ">":
            new_part_types = part_type.split(this.rating, this.value + 1)
            return {"pass": new_part_types[1], "fail": new_part_types[0]}
        else:
            print("Unknown test type: {}".format(this.type))
            exit()

class rule:
    def __init__(this, definition):
        if ":" not in definition:
            this.test = test("")
            this.destination = definition
        else:
            rule_text, this.destination = definition.split(":")
            this.test = test(rule_text)
    def apply(this, part_type):
        test_result = this.test.apply(part_type)
        return {"diverted_parts": test_result["pass"], "diverted_to": this.destination, "continuing_parts": test_result["fail"]}

class workflow:
    def __init__(this, definition):
        this.name, rule_defs = definition.strip()[:-1].split("{")
        this.rules = [rule(rule_def) for rule_def in rule_defs.split(",")]
    def apply(this, part_type):
        results = []
        continuing_part_type = part_type
        for rule in this.rules:
            rule_result = rule.apply(continuing_part_type)
            if rule_result["diverted_parts"] is not None:
                results.append({"destination": rule_result["diverted_to"], "parts": rule_result["diverted_parts"]})
            if rule_result["continuing_parts"] is not None:
                continuing_part_type = rule_result["continuing_parts"]
            else:
                break
        return results

all_workflows = {}

with open(input_file_name, encoding="utf-8") as input_file:
    workflow_defs, part_defs = input_file.read().strip().split("\n\n")
    for workflow_def in workflow_defs.split("\n"):
        w = workflow(workflow_def)
        all_workflows[w.name] = w

part_types_to_sort = [{"destination": "in", "parts": part_type()}]
accepted_part_types = []

while len(part_types_to_sort) > 0:
    pt = part_types_to_sort.pop()
    wf = all_workflows[pt["destination"]]
    wf_result = wf.apply(pt["parts"])
    for new_pt in wf_result:
        if new_pt["destination"] == "A":
            accepted_part_types.append(new_pt["parts"])
        elif new_pt["destination"] == "R":
            continue
        else:
            part_types_to_sort.append(new_pt)

rating_combo_sum = 0
for pt in accepted_part_types:
    rating_combo_sum += (pt.x_max - pt.x_min + 1) * (pt.m_max - pt.m_min + 1) * (pt.a_max - pt.a_min + 1) * (pt.s_max - pt.s_min + 1)

print(rating_combo_sum)