input_file_name = "input.txt"

with open(input_file_name, encoding="utf-8") as input_file:
    contraption = [line.strip() for line in input_file]

height = len(contraption)
width = len(contraption[0])

tiles_beam_directions_followed = [[[] for i in range(width)] for j in range(height)]

class beam:
    def __init__(this, row, column, direction):
        this.row = row
        this.column = column
        this.direction = direction

def follow_beam(b, queue):
    while b.row >= 0 and b.row < height and b.column >= 0 and b.column < width:
        if b.direction in tiles_beam_directions_followed[b.row][b.column]:
            return
        tiles_beam_directions_followed[b.row][b.column].append(b.direction)
        match contraption[b.row][b.column]:
            case "/":
                match b.direction:
                    case "R":
                        b.direction = "U"
                    case "D":
                        b.direction = "L"
                    case "L":
                        b.direction = "D"
                    case "U":
                        b.direction = "R"
            case "\\":
                match b.direction:
                    case "R":
                        b.direction = "D"
                    case "D":
                        b.direction = "R"
                    case "L":
                        b.direction = "U"
                    case "U":
                        b.direction = "L"
            case "|":
                if b.direction in "LR":
                    b.direction = "U"
                    queue.append(beam(b.row + 1, b.column, "D"))
            case "-":
                if b.direction in "UD":
                    b.direction = "L"
                    queue.append(beam(b.row, b.column + 1, "R"))
        match b.direction:
            case "R":
                b.column += 1
            case "D":
                b.row += 1
            case "L":
                b.column -= 1
            case "U":
                b.row -= 1

max_energized_tile_count = 0

def count_energized_tiles():
    count = 0
    for row in tiles_beam_directions_followed:
        for tile in row:
            if len(tile) > 0:
                count += 1
    return count

initial_beam_options = []

for i in range(width):
    initial_beam_options.append(beam(0,i,"D"))
    initial_beam_options.append(beam(height-1,i,"U"))

for i in range(height):
    initial_beam_options.append(beam(i,0,"R"))
    initial_beam_options.append(beam(i,width-1,"L"))

for ib in initial_beam_options:
    tiles_beam_directions_followed = [[[] for i in range(width)] for j in range(height)]
    beam_queue = [ib]
    while len(beam_queue) > 0:
        follow_beam(beam_queue.pop(), beam_queue)
    energized_tile_count = count_energized_tiles()
    if energized_tile_count > max_energized_tile_count:
        max_energized_tile_count = energized_tile_count

print(max_energized_tile_count)