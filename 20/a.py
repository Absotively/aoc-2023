input_file_name = "input.txt"

import json


class pulse:
    low_pulse_count = 0
    high_pulse_count = 0

    def __init__(this, source, destination, is_high, count=True):
        this.source = source
        this.destination = destination
        this.is_high = is_high
        if count:
            if is_high:
                pulse.high_pulse_count += 1
            else:
                pulse.low_pulse_count += 1
    
    def __str__(this):
        return "{} -{}-> {}".format(this.source, "high" if this.is_high else "low", this.destination)

pulse_queue = []

class module:
    def __init__(this, definition):
        identifier, outputs = definition.strip().split(" -> ")
        if identifier[0] == "%":
            this.type = "flip-flop"
            this.is_on = False
            this.name = identifier[1:]
        elif identifier[0] == "&":
            this.type = "conjunction"
            this.inputs = {}
            this.name = identifier[1:]
        else:
            this.type = "broadcaster"
            this.name = identifier
        # don't plan to actually create button module
        this.outputs = outputs.split(", ")

    def connect_input(this, input_name):
        if this.type == "conjunction":
            this.inputs[input_name] = pulse(input_name, this.name, False, False)

    def handle_pulse(this, p):
        if this.type == "flip-flop":
            if p.is_high:
                return
            else:
                this.is_on = not this.is_on
                for o in this.outputs:
                    pulse_queue.append(pulse(this.name, o, this.is_on))
        elif this.type == "conjunction":
            this.inputs[p.source] = p
            all_high = True
            for v in this.inputs.values():
                if not v.is_high:
                    all_high = False
                    break
            for o in this.outputs:
                pulse_queue.append(pulse(this.name, o, not all_high))
        else:
            # broadcaster
            for o in this.outputs:
                pulse_queue.append(pulse(this.name, o, p.is_high))

    # will use for comparisons, doesn't really need to be deserializable
    def serialize_state(this):
        if this.type == "flip-flop":
            return json.dumps(this.is_on)
        elif this.type == "conjunction":
            return json.dumps([(i.source, i.is_high) for i in this.inputs.values()])
        else:
            # broadcaster, has no saved state
            return ""

all_modules = {}

def serialize_all_modules_state():
    return json.dumps([(m.name, m.serialize_state()) for m in all_modules.values()])

with open(input_file_name, encoding="utf-8") as input_file:
    for line in input_file:
        m = module(line)
        all_modules[m.name] = m

for m in all_modules.values():
    for o in m.outputs:
        if o in all_modules:
            all_modules[o].connect_input(m.name)

button_press_count = 0
initial_state = serialize_all_modules_state()

while button_press_count < 1000 or len(pulse_queue) > 0:
    if button_press_count < 1000 and len(pulse_queue) == 0:
        # press button
        pulse_queue.append(pulse("", "broadcaster", False))
        button_press_count += 1
    p = pulse_queue.pop(0)
    if p.destination in all_modules:
        all_modules[p.destination].handle_pulse(p)
    if len(pulse_queue) == 0:
        state = json.dumps([(m.name, m.serialize_state()) for m in all_modules.values()])
        if state == initial_state:
            total_repeats = int(1000 / button_press_count)
            button_press_count *= total_repeats
            pulse.high_pulse_count *= total_repeats
            pulse.low_pulse_count *= total_repeats

print(pulse.high_pulse_count*pulse.low_pulse_count)