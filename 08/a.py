input_file_name = "input.txt"

directions = []
nodes = {}

with open(input_file_name, encoding="utf-8") as input_file:
  lines = input_file.readlines()
  directions = lines[0].strip()
  for node_line in lines[2:]:
    name = node_line[0:3]
    left = node_line[7:10]
    right = node_line[12:15]
    nodes[name] = (left, right)

location = "AAA"
steps = 0
step_index = 0

while location != "ZZZ":
  if directions[step_index] == "L":
    location = nodes[location][0]
  else:
    location = nodes[location][1]
  steps += 1
  step_index = (step_index + 1) % len(directions)

print(steps)