input_file_name = "input.txt"

directions = []
direction_count = 0
nodes = {}

with open(input_file_name, encoding="utf-8") as input_file:
  lines = input_file.readlines()
  directions = lines[0].strip()
  direction_count = len(directions)
  for node_line in lines[2:]:
    name = node_line[0:3]
    left = node_line[7:10]
    right = node_line[12:15]
    nodes[name] = (left, right)

all_route_data = []

def check_if_arrived(current_locations):
  for l in current_locations:
    if l[2] != "Z":
      return False
  return True

for l in nodes.keys():
  if l[2] == "A":
    route_data = {"name": l}
    route_data["destinations"] = {}
    route_steps = 0
    step_index = 0
    found_cycle = False
    while not found_cycle:
      if directions[step_index] == "L":
        l = nodes[l][0]
      else:
        l = nodes[l][1]
      route_steps += 1
      step_index = (step_index + 1) % direction_count
      if l[2] == "Z":
        if l not in route_data["destinations"]:
          route_data["destinations"][l] = []
        route_data["destinations"][l].append(route_steps)
        if len(route_data["destinations"][l]) > 1:
          if (route_steps - route_data["destinations"][l][0]) % direction_count == 0:
            found_cycle = True
            route_data["cycle_length"] = route_steps - route_data["destinations"][l][0]
            route_data["first_destination_steps"] = route_data["destinations"][l][0]
    all_route_data.append(route_data)

# confirm what skimming the route data suggested
for rd in all_route_data:
  if rd["cycle_length"] != rd["first_destination_steps"]:
    print("found route where first destination not equal to cycle length")

# so then need least common multiple of cycle lengths
# basically copying https://www.w3schools.com/python/numpy/numpy_ufunc_lcm.asp

import numpy as np

x = np.lcm.reduce(np.ulonglong([r["cycle_length"] for r in all_route_data]))

print(x) 