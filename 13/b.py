input_file_name = "input.txt"

map_summary = 0

with open(input_file_name, encoding="utf-8") as input_file:
  raw_maps = input_file.read().split("\n\n")
  for raw_map in raw_maps:
    map = raw_map.split("\n")
    if map[-1] == "":
      map.pop()

    # look for horizontal mirror
    found_mirror = False
    for i in range(len(map) - 1):
      smudges_needed_count = 0
      for j in range(0, min(i + 1, len(map) - i - 1)):
        row_a = map[i-j]
        row_b = map[i+j+1]
        for k in range(len(row_a)):
          if row_a[k] != row_b[k]:
            smudges_needed_count += 1
            if smudges_needed_count > 1:
              break
        if smudges_needed_count > 1:
          break
      if smudges_needed_count == 1:
        map_summary += (i+1) * 100
        found_mirror = True
        #print("found mirror below row {}".format(i+1))
        continue
    
    # if we didn't find one, look for a vertical mirror
    if not found_mirror:
      map_transposed = [[line[i] for line in map] for i in range(len(map[0]))]
      
      for i in range(len(map_transposed) - 1):
        smudges_needed_count = 0
        for j in range(0, min(i + 1, len(map_transposed) - i - 1)):
          column_a = map_transposed[i-j]
          column_b = map_transposed[i+j+1]
          for k in range(len(column_a)):
            if column_a[k] != column_b[k]:
              smudges_needed_count += 1
              if smudges_needed_count > 1:
                break
          if smudges_needed_count > 1:
            break
        if smudges_needed_count == 1:
          map_summary += i+1
          found_mirror = True
          #print("found mirror to right of column {}".format(i+1))
          continue

print(map_summary)