input_file_name = "input.txt"

def is_all_zeroes(sequence):
  for n in sequence:
    if n != 0:
      return False
  return True

sum = 0

with open(input_file_name, encoding="utf-8") as input_file:
  for line in input_file:
    sequences = [[int(n) for n in line.strip().split(" ")]]
    while not is_all_zeroes(sequences[-1]):
      prev_seq = sequences[-1]
      seq = []
      for i in range(len(prev_seq) - 1):
        seq.append(prev_seq[i+1] - prev_seq[i])
      sequences.append(seq)

    sequences[-1].insert(0,0)
    for i in range(len(sequences) - 2, -1, -1):
      sequences[i].insert(0, sequences[i][0] - sequences[i+1][0])

    sum += sequences[0][0]

print(sum)

# 958478422 is too low