input_file_name = "input.txt"

from collections import Counter

class hand_type:
  def __init__(this, name, sort_key):
    this.name = name
    this.sort_key = sort_key
  def __repr__(this):
    return 'hand_type({}, {})'.format(this.name, this.sort_key)
  def __str__(this):
    return 'Hand type: {}'.format(this.name)

def count_cards(cards):
  c = Counter(cards)
  joker_count = c["J"]
  del c["J"]
  counts = sorted(list(c.values()))
  if len(counts) == 0:
    return [5]
  counts[len(counts) - 1] += joker_count
  return counts

card_ranks = "J23456789TQKA"

class hand:
  def __init__(this, cards, bid):
    this.cards = cards
    this.bid = bid
    counts = count_cards(cards)
    match counts:
      case [5]:
        this.type = hand_type("Five of a kind", 7)
      case [1,4]:
        this.type = hand_type("Four of a kind", 6)
      case [2,3]:
        this.type = hand_type("Full house", 5)
      case [1,1,3]:
        this.type = hand_type("Three of a kind", 4)
      case [1,2,2]:
        this.type = hand_type("Two pair", 3)
      case [1,1,1,2]:
        this.type = hand_type("One pair", 2)
      case _:
        this.type = hand_type("High card", 1)
        

  def __repr__(this):
    return 'hand({}, {})'.format(this.cards, this.bid)
  def __str__(this):
    return '{} ({}) (bid: {})'.format(this.cards, this.type.name, this.bid)
  def __lt__(self, other):
    if self.type.sort_key != other.type.sort_key:
      return self.type.sort_key < other.type.sort_key
    for i in range(5):
      if self.cards[i] != other.cards[i]:
        return card_ranks.index(self.cards[i]) < card_ranks.index(other.cards[i])

hands = []

with open(input_file_name, encoding="utf-8") as input_file:
  for line in input_file:
    hands.append(hand(line[0:5], int(line[6:].strip())))

hands.sort()

sum = 0

for i in range(len(hands)):
  sum += (i + 1) * hands[i].bid

print(sum)

# 250426588 is too high
# 249153803 is too high