input_file_name = "input.txt"

step_goal = 26501365

with open(input_file_name, encoding="utf-8") as input_file:
    farm_map = [line.strip() for line in input_file]

for i in range(len(farm_map)):
    if "S" in farm_map[i]:
        column = farm_map[i].index("S")
        newly_visited_positions = set([(i, column)])
        break

width = len(farm_map[0])
height = len(farm_map)

step_count = 0
# assuming that step goal is even, which is fine because it is
visited_positions_even = set()
visited_positions_all = set()

update = 1000

while step_count < step_goal:
    new_new_positions = set()
    for p in newly_visited_positions:
        for vec in [(0,1),(0,-1),(1,0),(-1,0)]:
            n = (p[0] + vec[0], p[1] + vec[1])
            if (farm_map[n[0] % height][n[1] % width] in "S."
                and n not in visited_positions_all):
                new_new_positions.add(n)
    newly_visited_positions = new_new_positions
    visited_positions_all |= new_new_positions
    step_count += 1
    if step_count % 2 == 0:
        visited_positions_even |= new_new_positions
    if step_count % update == 0:
        print(step_count)

print(len(visited_positions_even))
