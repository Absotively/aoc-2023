input_file_name = "input.txt"

step_goal = 64

with open(input_file_name, encoding="utf-8") as input_file:
    farm_map = [line.strip() for line in input_file]

for i in range(len(farm_map)):
    if "S" in farm_map[i]:
        column = farm_map[i].index("S")
        current_positions = set([(i, column)])
        break

width = len(farm_map[0])
height = len(farm_map)

step_count = 0

while step_count < step_goal:
    new_positions = set()
    for p in current_positions:
        for vec in [(0,1),(0,-1),(1,0),(-1,0)]:
            n = (p[0] + vec[0], p[1] + vec[1])
            if (n[0] >= 0 and n[0] < height
                 and n[1] >= 0 and n[1] < width
                 and farm_map[n[0]][n[1]] in "S."):
                new_positions.add(n)
    current_positions = new_positions
    step_count += 1

print(len(current_positions))
