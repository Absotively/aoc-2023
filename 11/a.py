input_file_name = "input.txt"

image = []

with open(input_file_name, encoding="utf-8") as input_file:
  for line in input_file:
    image.append(line.strip())
    if "#" not in line:
      image.append(image[-1])

def transpose_image(source):
  return [[row[i] for row in source] for i in range(len(source[0]))]

temp = transpose_image(image)
temp_processed = []
for row in temp:
  temp_processed.append(row)
  if "#" not in row:
    temp_processed.append(row)

image = transpose_image(temp_processed)

galaxies = []

for row in range(len(image)):
  for column in range(len(image[0])):
    if image[row][column] == "#":
      galaxies.append((row, column))

sum = 0

# routes!
for i in range(len(galaxies) - 1):
  g1 = galaxies[i]
  for g2 in galaxies[i+1:]:
    sum += abs(g1[0] - g2[0]) + abs(g1[1] - g2[1])

print(sum)

