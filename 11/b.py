input_file_name = "input.txt"

image = []
empty_rows = []
empty_columns = []

with open(input_file_name, encoding="utf-8") as input_file:
  i = 0
  for line in input_file:
    image.append(line.strip())
    if "#" not in line:
      empty_rows.append(i)
    i += 1

def transpose_image(source):
  return [[row[i] for row in source] for i in range(len(source[0]))]

temp = transpose_image(image)
for i in range(len(temp)):
  if "#" not in temp[i]:
    empty_columns.append(i)

galaxies = []

current_actual_row = 0
for row in range(len(image)):
  current_actual_column = 0
  for column in range(len(image[0])):
    if image[row][column] == "#":
      galaxies.append((current_actual_row, current_actual_column))
    if column in empty_columns:
      current_actual_column += 1000000
    else:
      current_actual_column += 1
  if row in empty_rows:
    current_actual_row += 1000000
  else:
    current_actual_row += 1

sum = 0

# routes!
for i in range(len(galaxies) - 1):
  g1 = galaxies[i]
  for g2 in galaxies[i+1:]:
    sum += abs(g1[0] - g2[0]) + abs(g1[1] - g2[1])

print(sum)

