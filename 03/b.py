import pprint

input_file_name = "input.txt"

sum = 0
input = None
gear_adjacent_numbers_by_gear = {}

def add_gear_adjacent_number(gear_row, gear_column, number):
  if (gear_row, gear_column) not in gear_adjacent_numbers_by_gear:
    gear_adjacent_numbers_by_gear[(gear_row, gear_column)] = []
  gear_adjacent_numbers_by_gear[(gear_row, gear_column)].append(number)

with open(input_file_name, encoding="utf-8") as input_file:
  input = input_file.readlines()

# newlines
input = [l.strip() for l in input]
  
def is_actual_digit(d):
  return d in ["0","1","2","3","4","5","6","7","8","9"]

for i in range(len(input)):
  line = input[i]
  num_start = None
  for j in range(len(line)):
    if is_actual_digit(line[j]):
      if num_start == None:
        num_start = j
    if (not is_actual_digit(line[j]) or j + 1 == len(line)) and num_start is not None:
      if j + 1 == len(line) and is_actual_digit(line[j]):
        # handle line-end number
        j += 1
      num = int(line[num_start:j])
      if num_start > 0 and line[num_start - 1] == "*":
        add_gear_adjacent_number(i, num_start - 1, num)
      elif j < len(line) and line[j] == "*":
        add_gear_adjacent_number(i, j, num)
      else:
        for k in range(num_start - 1, j + 1):
          if i > 0 and k >= 0 and k < len(input[i-1]) and input[i-1][k] == "*":
            add_gear_adjacent_number(i-1, k, num)
            break
          if i + 1 < len(input) and k >= 0 and k < len(input[i+1]) and input[i+1][k] == "*":
            add_gear_adjacent_number(i+1, k, num)
            break
      num_start = None

for num_list in gear_adjacent_numbers_by_gear.values():
  if len(num_list) == 2:
    sum += num_list[0] * num_list[1]

print(sum)

# 68286950 is too low