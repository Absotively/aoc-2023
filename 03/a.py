input_file_name = "input.txt"

sum = 0
input = None

with open(input_file_name, encoding="utf-8") as input_file:
  input = input_file.readlines()

# newlines
input = [l.strip() for l in input]
  
def is_actual_digit(d):
  return d in ["0","1","2","3","4","5","6","7","8","9"]

for i in range(len(input)):
  line = input[i]
  num_start = None
  for j in range(len(line)):
    if is_actual_digit(line[j]):
      if num_start == None:
        num_start = j
    if (not is_actual_digit(line[j]) or j + 1 == len(line)) and num_start is not None:
      if j + 1 == len(line) and is_actual_digit(line[j]):
        # handle line-end number
        j += 1
      num = int(line[num_start:j])
      is_part_num = False
      if num_start > 0 and line[num_start - 1] != ".":
        is_part_num = True
      elif j < len(line) and line[j] != ".":
        is_part_num = True
      else:
        for k in range(num_start - 1, j + 1):
          if i > 0 and k >= 0 and k < len(input[i-1]) and input[i-1][k] != "." and not is_actual_digit(input[i-1][k]):
            is_part_num = True
            break
          if i + 1 < len(input) and k >= 0 and k < len(input[i+1]) and input[i+1][k] != "." and not is_actual_digit(input[i+1][k]):
            is_part_num = True
            break
      if is_part_num:
        sum += num
      num_start = None

print(sum)

# 512564 is too low    
# 531543 is too low