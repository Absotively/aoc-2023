import functools

input_file_name = "input.txt"

arrangements_sum = 0

@functools.cache
def count_possible_arrangements(record, groups):
  unprocessed_record = record
  unfound_groups = groups
  arrangements = 0
  while len(unprocessed_record) > 0 and len(unfound_groups) > 0 and unprocessed_record[0] != "?":
    if sum(unfound_groups) + len(unfound_groups) - 1 > len(unprocessed_record):
        return 0
    if unprocessed_record[0] == "#":
      # next springs have to be the rest of the damaged group, then an
      # undamaged spring if we're not at the end of the row
      if "." in unprocessed_record[:unfound_groups[0]]:
        return 0
      if len(unprocessed_record) > unfound_groups[0] and unprocessed_record[unfound_groups[0]] == "#":
        # group too long
        return 0
      elif unfound_groups[0] + 1 >= len(unprocessed_record):
        return 1
      else:
        unprocessed_record = unprocessed_record[unfound_groups[0] + 1:]
        unfound_groups = unfound_groups[1:]
    else:
      # new_record[i] must be "." by process of elimination
      unprocessed_record = unprocessed_record[1:]

  if len(unprocessed_record) == 0 and len(unfound_groups) > 0:
    # nowhere for remaining groups to be
    return 0
  if len(unfound_groups) == 0:
    if "#" in unprocessed_record:
      return 0
    else:
      return 1
  
  arrangements = count_possible_arrangements("." + unprocessed_record[1:], unfound_groups)
  arrangements += count_possible_arrangements("#" + unprocessed_record[1:], unfound_groups)
  return arrangements

with open(input_file_name, encoding="utf-8") as input_file:
  for line in input_file:
    folded_record, folded_groups = line.strip().split(" ")
    record = "?".join([folded_record for i in range(5)])
    folded_groups = [int(n) for n in folded_groups.split(",")]
    groups = tuple(folded_groups + folded_groups + folded_groups + folded_groups + folded_groups)
    arrangements_sum += count_possible_arrangements(record, groups)

print(arrangements_sum)
