input_file_name = "input.txt"

sum = 0

with open(input_file_name, encoding="utf-8") as input_file:
  for line in input_file:
    minimums = {
      "red": 0,
      "green": 0,
      "blue": 0
    }
    colon_pos = line.find(":")
    game_id = int(line[5:colon_pos])
    handfuls = line[colon_pos + 2:].split("; ")
    for handful in handfuls:
      handful = handful.strip()
      cube_counts = handful.split(", ")
      for cube_count in cube_counts:
        (count_string, color) = cube_count.split(" ")
        count = int(count_string)
        if count > minimums[color]:
          minimums[color] = count
    sum += minimums["red"] * minimums["green"] * minimums["blue"]

print(sum)