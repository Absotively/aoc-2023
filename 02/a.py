input_file_name = "input.txt"

maximums = {
"red": 12,
"green": 13,
"blue": 14
}

sum = 0

with open(input_file_name, encoding="utf-8") as input_file:
  for line in input_file:
    colon_pos = line.find(":")
    game_id = int(line[5:colon_pos])
    handfuls = line[colon_pos + 2:].split("; ")
    went_over = False
    for handful in handfuls:
      handful = handful.strip()
      cube_counts = handful.split(", ")
      for cube_count in cube_counts:
        (count_string, color) = cube_count.split(" ")
        count = int(count_string)
        if count > maximums[color]:
          went_over = True
          continue
      if went_over:
        continue
    if not went_over:
      sum += game_id

print(sum)