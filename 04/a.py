input_file_name = "input.txt"

sum = 0

with open(input_file_name, encoding="utf-8") as input_file:
  for line in input_file:
    (winning, present) = line[10:].split(" | ")
    winning_numbers = [int(s) for s in winning.strip().split(" ") if s != '']
    present_numbers = [int(s) for s in present.strip().split(" ") if s != '']
    match_count = 0
    for num in present_numbers:
      if num in winning_numbers:
        match_count += 1
    if match_count > 0:
      sum += 2 ** (match_count - 1)

print(sum)