input_file_name = "input.txt"

card_copies = {}
def add_copy(card_num, count = 1):
  if card_num in card_copies:
    card_copies[card_num] += count
  else:
    card_copies[card_num] = count

total_copies = 0

with open(input_file_name, encoding="utf-8") as input_file:
  for line in input_file:
    card_num = int(line[4:8].strip())
    add_copy(card_num)

    (winning, present) = line[10:].split(" | ")
    winning_numbers = [int(s) for s in winning.strip().split(" ") if s != '']
    present_numbers = [int(s) for s in present.strip().split(" ") if s != '']
    match_count = 0
    for num in present_numbers:
      if num in winning_numbers:
        match_count += 1
    for i in range(match_count):
      add_copy(card_num + i + 1, card_copies[card_num])

    total_copies += card_copies[card_num]
        

print(total_copies)