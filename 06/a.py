# input too short to bother parsing
input = {
    "time": [56, 71, 79, 99],
    "distance": [334, 1135, 1350, 2430]
}

product = 1

for i in range(len(input["time"])):
    time = input["time"][i]
    distance = input["distance"][i]

    # I assume I'll have to do something more complicated for part two,
    # but this is not part two
    ways_to_win = 0
    for j in range(time):
        if j * (time - j) > distance:
            ways_to_win += 1
    
    product *= ways_to_win

print(product)