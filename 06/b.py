# input too short to bother parsing
time = 56717999
distance = 334113513502430

def test_button_time(t):
    return t * (time - t) > distance

# plan: find any one working value, then do binary search for the thresholds
pivot = 1
division = 2
found_good_pivot = False
while not found_good_pivot:
    for i in range(1, division):
        pivot = i * int(time / division)
        if test_button_time(pivot):
            found_good_pivot = True
            break
    division += 1

# binary search for lower threshold
below_threshold = 1
threshold_max = pivot
lower_threshold = None
while lower_threshold is None:
    test_value = below_threshold + int((threshold_max - below_threshold) / 2)
    if test_button_time(test_value):
        threshold_max = test_value
    else:
        below_threshold = test_value
    if below_threshold + 1 == threshold_max:
        lower_threshold = threshold_max

# binary search for upper threshold
above_threshold = time
threshold_min = pivot
upper_threshold = None
while upper_threshold is None:
    test_value = above_threshold + int((threshold_min - above_threshold) / 2)
    if test_button_time(test_value):
        threshold_min = test_value
    else:
        above_threshold = test_value
    if above_threshold - 1 == threshold_min:
        upper_threshold = threshold_min

print(upper_threshold - lower_threshold + 1)