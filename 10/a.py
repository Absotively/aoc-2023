input_file_name = "input.txt"

class pipe:
  def __init__(this, type):
    this.type = type
    match type:
      case "|":
        this.connections = "NS"
      case "-":
        this.connections = "EW"
      case "L":
        this.connections = "NE"
      case "J":
        this.connections = "NW"
      case "7":
        this.connections = "SW"
      case "F":
        this.connections = "SE"
      case _:
        this.connections = ""
  def can_navigate(this, from_direction):
    return from_direction in this.connections
  def navigate(this, from_direction):
    if this.connections[0] == from_direction:
      return this.connections[1]
    else:
      return this.connections[0]

field = []
starting_row = None
starting_column = None

with open(input_file_name, encoding="utf-8") as input_file:
  for line in input_file:
    row = [pipe(c) for c in line]
    field.append(row)
    if "S" in line:
      starting_row = len(field) - 1
      starting_column = line.index("S")

class location:
  def __init__(this, row, column, direction_entered_from):
    this.row = row
    this.column = column
    this.direction_entered_from = direction_entered_from
  def __repr__(this):
    return "location({}, {}, {})".format(this.row, this.column, this.direction_entered_from)

current_locations = []

# step 1
surroundings = [(-1,0,"S"),(0,1,"W"),(1,0,"N"),(0,-1,"E")]
for s in surroundings:
  row = starting_row + s[0]
  column = starting_column + s[1]
  if (row > 0 and row < len(field)
       and column > 0 and column < len(field[0])
       and field[row][column].can_navigate(s[2])):
    current_locations.append(location(row, column, s[2]))

steps = 1

# remaining steps
while (current_locations[0].row != current_locations[1].row
       or current_locations[0].column != current_locations[1].column):
  for i in (0,1):
    l = current_locations[i]
    match field[l.row][l.column].navigate(l.direction_entered_from):
      case "N":
        current_locations[i] = location(l.row - 1, l.column, "S")
      case "E":
        current_locations[i] = location(l.row, l.column + 1, "W")
      case "S":
        current_locations[i] = location(l.row + 1, l.column, "N")
      case "W":
        current_locations[i] = location(l.row, l.column - 1, "E")
  steps += 1

print(steps)