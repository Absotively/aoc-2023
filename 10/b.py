input_file_name = "input.txt"

class pipe:
  def __init__(this, type):
    this.type = type
    match type:
      case "|":
        this.connections = "NS"
        this.display = "┃"
      case "-":
        this.connections = "EW"
        this.display = "━"
      case "L":
        this.connections = "NE"
        this.display = "┗"
      case "J":
        this.connections = "NW"
        this.display = "┛"
      case "7":
        this.connections = "SW"
        this.display = "┓"
      case "F":
        this.connections = "SE"
        this.display = "┏"
      case _:
        this.connections = ""
        this.display = this.type
  def can_navigate(this, from_direction):
    return from_direction in this.connections
  def navigate(this, from_direction):
    if this.connections[0] == from_direction:
      return this.connections[1]
    else:
      return this.connections[0]

field = []
starting_row = None
starting_column = None

with open(input_file_name, encoding="utf-8") as input_file:
  for line in input_file:
    row = [pipe(c) for c in line]
    field.append(row)
    if "S" in line:
      starting_row = len(field) - 1
      starting_column = line.index("S")

class location:
  def __init__(this, row, column, direction_entered_from):
    this.row = row
    this.column = column
    this.direction_entered_from = direction_entered_from
  def __repr__(this):
    return "location({}, {}, {})".format(this.row, this.column, this.direction_entered_from)

starting_pipes = []

# step 1
surroundings = [(-1,0,"S"),(0,1,"W"),(1,0,"N"),(0,-1,"E")]
for s in surroundings:
  row = starting_row + s[0]
  column = starting_column + s[1]
  if (row > 0 and row < len(field)
       and column > 0 and column < len(field[0])
       and field[row][column].can_navigate(s[2])):
    starting_pipes.append(location(row, column, s[2]))

starting_pipe_is_vertical = (starting_pipes[0].direction_entered_from in "NS" and starting_pipes[1].direction_entered_from in "NS")

pipe_in_loop = [[False for i in range(len(field[0]))] for j in range(len(field))]
pipe_in_loop[starting_row][starting_column] = True

current_location = starting_pipes[0]

# find all loop segments
while (current_location.row != starting_row
       or current_location.column != starting_column):
    l = current_location
    pipe_in_loop[l.row][l.column] = True
    match field[l.row][l.column].navigate(l.direction_entered_from):
      case "N":
        current_location = location(l.row - 1, l.column, "S")
      case "E":
        current_location = location(l.row, l.column + 1, "W")
      case "S":
        current_location = location(l.row + 1, l.column, "N")
      case "W":
        current_location = location(l.row, l.column - 1, "E")

# output loop for debugging reasons, make it look pretty for "J doesn't look like a corner in my font" reasons
if True:
  with open("loop.txt", 'w', encoding="utf-8") as loop_file:
    for i in range(len(field)):
      for j in range(len(field[i])):
        if pipe_in_loop[i][j]:
          loop_file.write(field[i][j].display)
        else:
          loop_file.write(" ")
      loop_file.write("\n")

enclosed_tile_count = 0

# count squares that are not part of the pipe and have an odd number of
# vertical pipe segments between them and the W end of their row
for i in range(len(field)):
  row = field[i]
  pipe_segments = pipe_in_loop[i]
  inside_loop = False
  for j in range(len(row)):
    if pipe_segments[j]:
      if row[j].type in "|JL":
        # "-" doesn't affect whether in loop on this row;
        # "F" and "7" only affect it if paired with a "J" or "L"
        inside_loop = not inside_loop
    elif inside_loop:
      enclosed_tile_count += 1

print(enclosed_tile_count)

# 1507 is too high