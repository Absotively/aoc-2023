input_file_name = "input.txt"

with open(input_file_name, encoding="utf-8") as input_file:
    init_sequence = input_file.read()

if init_sequence[-1] == "\n":
    init_sequence = init_sequence[:-1]

def HASH(s):
    result = 0
    for c in s:
        result = ((result + ord(c)) * 17) % 256
    return result

boxes = [[] for i in range(256)]

class lens:
    def __init__(this, label, focal_length):
        this.label = label
        this.focal_length = focal_length
    def __repr__(this):
        return "lens({},{})".format(this.label, this.focal_length)
    def __str__(this):
        return "{} {}".format(this.label, this.focal_length)

for step in init_sequence.split(","):
    if step.endswith("-"):
        label = step[:-1]
        box = HASH(label)
        for i in range(len(boxes[box])):
            if boxes[box][i].label == label:
                del boxes[box][i]
                break
    else:
        label = step[:-2]
        box = HASH(label)
        focal_length = int(step[-1])
        new_lens = lens(label, focal_length)
        lens_inserted = False
        for i in range(len(boxes[box])):
            if boxes[box][i].label == label:
                boxes[box][i] = new_lens
                lens_inserted = True
                break
        if not lens_inserted:
            boxes[box].append(new_lens)

total_focusing_power = 0

for i in range(256):
    for j in range(len(boxes[i])):
        total_focusing_power += (1 + i) * (1 + j) * boxes[i][j].focal_length

print(total_focusing_power)

# 269336 is too low
