input_file_name = "input.txt"

with open(input_file_name, encoding="utf-8") as input_file:
    init_sequence = input_file.read()

if init_sequence[-1] == "\n":
    init_sequence = init_sequence[:-1]

def HASH(s):
    result = 0
    for c in s:
        result = ((result + ord(c)) * 17) % 256
    return result

hash_sum = 0

for step in init_sequence.split(","):
    hash_sum += HASH(step)

print(hash_sum)