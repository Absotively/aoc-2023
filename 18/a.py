input_file_name = "input.txt"

# True = dug out as part of perimeter
rows = {}
rows[0] = {0: True}

position = (0,0)
direction_vectors = {"U": (-1,0), "R":(0,1), "D":(1,0), "L":(0,-1)}

with open(input_file_name, encoding="utf-8") as input_file:
    for line in input_file:
        direction, distance, colour = line.strip().split(" ")
        v = direction_vectors[direction]
        for i in range(int(distance)):
            position = (position[0] + v[0], position[1] + v[1])
            if position[0] not in rows:
                rows[position[0]] = {}
            rows[position[0]][position[1]] = True

# convert the data into a nice array
top_row = min(rows.keys())
bottom_row = max(rows.keys())
downward_shift = top_row * -1

left_column = 0
right_column = 0
for r in rows.values():
    left_column = min(left_column, min(r.keys()))
    right_column = max(right_column, max(r.keys()))
rightward_shift = left_column * -1

plan_array = [["." for i in range(left_column, right_column + 1)] for j in range(top_row, bottom_row + 1)]

for i in range(top_row, bottom_row + 1):
    for j in range(left_column, right_column + 1):
        if j in rows[i]:
            plan_array[i + downward_shift][j + rightward_shift] = "#"

# print("\n".join(["".join(r) for r in plan_array]))
# confirmed by inspecting above output that trench never doubles back directly next to itself, thankfully

# finish digging out lagoon
            
for i in range(len(plan_array)):
    in_lagoon = False
    j = 0
    while j < len(plan_array[i]):
        if plan_array[i][j] == "#":
            k = j + 1
            while k < len(plan_array[i]) and plan_array[i][k] == "#":
                k += 1
            if k - j == 1:
                in_lagoon = not in_lagoon
            elif i == 0:
                in_lagoon = False   # Top row can only have outer trench
            elif k < len(plan_array[i]):
                in_lagoon = (plan_array[i-1][k] == "#")
            j = k
        if in_lagoon and j < len(plan_array[i]):
            plan_array[i][j] = "#"
        j += 1

# print("\n".join(["".join(r) for r in plan_array]))
        
print(sum([row.count("#") for row in plan_array]))