input_file_name = "input.txt"

direction_vectors = {"3": (-1,0), "0":(0,1), "1":(1,0), "2":(0,-1)}

class trench_edge:
    def __init__(this, direction, distance, starting_row, starting_column):
        this.direction = direction
        this.distance = distance
        this.starting_row = starting_row
        this.starting_column = starting_column
        this.ending_row = starting_row + direction_vectors[direction][0] * distance
        this.ending_column = starting_column + direction_vectors[direction][1] * distance 
        if this.starting_row > this.ending_row:
            this.starting_row, this.ending_row = this.ending_row, this.starting_row
        if this.starting_column > this.ending_column:
            this.starting_column, this.ending_column = this.ending_column, this.starting_column

    def portion_in_row(this, row):
        if row >= this.starting_row and row <= this.ending_row:
            return (this.starting_column, this.ending_column)
        return None

    def __repr__(this):
        return("({},{})-({},{})".format(this.starting_row, this.starting_column, this.ending_row, this.ending_column))

edges = []
position = (0,0)
top_row = 0
bottom_row = 0
left_column = 0
right_column = 0
interesting_rows = set([0])

with open(input_file_name, encoding="utf-8") as input_file:
    for line in input_file:
        direction = line[-3]
        distance = int(line[-8:-3], base=16)
        v = direction_vectors[direction]
        edges.append(trench_edge(direction, distance, position[0], position[1]))
        position = (position[0] + v[0] * distance, position[1] + v[1] * distance)
        top_row = min(top_row, position[0])
        bottom_row = max(bottom_row, position[0])
        left_column = min(left_column, position[1])
        right_column = max(right_column, position[1])
        interesting_rows.add(position[0])

def clean_segments(segments):
    cleaned_segment_count = 0
    while cleaned_segment_count + 1 < len(segments):
        if segments[cleaned_segment_count][1] >= segments[cleaned_segment_count+1][0]:
            segments[cleaned_segment_count:cleaned_segment_count+2] = [(segments[cleaned_segment_count][0], segments[cleaned_segment_count+1][1])]
        else:
            cleaned_segment_count += 1

# assuming that the outer trench still doesn't double back directly next to itself

lagoon_segments_by_row = {}
lagoon_tile_count = 0
interesting_rows = sorted(list(interesting_rows))

# uninteresting rows are easier so do them first
for i in range(len(interesting_rows) - 1):
    starting_row = interesting_rows[i] + 1
    ending_row = interesting_rows[i+1] - 1
    if starting_row > ending_row:
        continue
    trench_segments = []
    for edge in edges:
        edge_portion = edge.portion_in_row(starting_row)
        if edge_portion is not None:
            trench_segments.append(edge_portion) # if it wasn't vertical then starting_row would be interesting
    trench_segments.sort(key = lambda s: s[0])
    lagoon_segments = []
    for j in range(int(len(trench_segments) / 2)):
        lagoon_segments.append((trench_segments[2*j][0],trench_segments[2*j+1][1]))
    lagoon_tiles_per_row = sum([s[1] - s[0] + 1 for s in lagoon_segments])
    lagoon_tile_count += lagoon_tiles_per_row * (ending_row - starting_row + 1)
    lagoon_segments_by_row[ending_row] = lagoon_segments

for i in interesting_rows:
    trench_segments = []
    for edge in edges:
        edge_portion = edge.portion_in_row(i)
        if edge_portion is not None:
            trench_segments.append(edge_portion)
    trench_segments.sort(key = lambda s: s[1])
    trench_segments.sort(key = lambda s: s[0])
    clean_segments(trench_segments)
    lagoon_segments = []
    trench_segment_number = 0
    while trench_segment_number < len(trench_segments):
        trench_segment = trench_segments[trench_segment_number]
        if i == top_row:
            lagoon_segments.append(trench_segment)
        else:
            next_column = trench_segment[1] + 1
            segment_found_above = False
            for s in lagoon_segments_by_row[i-1]:
                if s[0] <= next_column and s[1] >= next_column:
                    segment_found_above = True
                    break
            if segment_found_above:
                lagoon_segments.append((trench_segment[0], trench_segments[trench_segment_number + 1][1]))
            else:
                lagoon_segments.append(trench_segment)
        trench_segment_number += 1
    
    clean_segments(lagoon_segments)

    lagoon_segments_by_row[i] = lagoon_segments
    for s in lagoon_segments:
        lagoon_tile_count += (s[1] - s[0] + 1)

print(lagoon_tile_count)

# 40654918188460 is too low